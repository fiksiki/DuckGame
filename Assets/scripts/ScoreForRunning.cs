using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreForRunning : MonoBehaviour
{
    public Text score;
    public Text score2;
    public float cur_score;
	int score_to_display;

    void Update()
    {
	    //Обновляет счет игрока и показывает его
	    cur_score += Time.deltaTime*10;
	    int score_to_display = (int)cur_score;
	    score.text = score_to_display.ToString();
	    score2.text = score_to_display.ToString();

    }
}
