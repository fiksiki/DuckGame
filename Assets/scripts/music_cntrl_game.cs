using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music_cntrl_game: MonoBehaviour
{
    // настройка громкости фоновой музыки в игре и сопросождения источником звука игрока
    public AudioSource bg_sound;
    private float musicVolume = 1f;
    public GameObject player;

    void Start()
    {
        bg_sound = GetComponent<AudioSource>();
    }

    void Update()
    {
        bg_sound.volume = musicVolume;
        transform.position = new Vector3(player.transform.position.x+5, player.transform.position.y, 0);
    }

    public void SetVolume(float volume)
    {
        musicVolume = volume;
    }
}   
