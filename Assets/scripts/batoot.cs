using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class batoot : MonoBehaviour
{
	public GameObject utka;
	private AudioSource bg_sound;
	void Start()
	{
		bg_sound = GetComponent<AudioSource>();
	}
    // При столкновении с игроком, подкинуть его и произвести звук батута
        void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player"){
			col.GetComponent<Rigidbody2D>().velocity= new Vector2(col.GetComponent<Rigidbody2D>().velocity.x, 15);
			bg_sound.volume = PlayerPrefs.GetFloat("savevolume");
			bg_sound.Play();
		}
}
}
