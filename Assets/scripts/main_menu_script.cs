using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class main_menu_script : MonoBehaviour
{
	//Кнопки для главного меню
	public void PlayPressed()
	{
		SceneManager.LoadScene("Game_scene");
	}

	public void ExitPressed()
	{
		Application.Quit();
	}
}
