using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class player_cntrl : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    private float moveInput;

    private int health;

    private Rigidbody2D rb;
    private bool is_on_ground;
    public Transform feetPos;
    public float checkRadius = 0.5f;
    public LayerMask WhatIsGround;

    private Animator anim;

    public GameObject Game;
    public GameObject UserInterfaceCanvas;
    public GameObject DeathCanvas;
	public GameObject healthIcon;
	public GameObject RealScoreTable;
    
    private int score_to_display_temp;
    public AudioSource barkSound;
    public AudioSource eatSound;  
	public AudioSource deathSound;  
	public AudioSource bolnoSound;    

	private int score;
    private int record;  
   

    private void Start()
    {
	    // получение компонентов персонажа
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        health = 1;
        
    }

    public void Jump(int height)
    {
	    rb.velocity = new Vector2(rb.velocity.x, height);
    }
    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.tag == "ExtraLife")
        {
	        // Получение доп жизни, воспроизведение соотв. звука и удаление самого бонуса
			eatSound.volume = PlayerPrefs.GetFloat("savevolume");
            eatSound.Play(); 
            health = 2;
            Destroy(obj.gameObject);
			healthIcon.SetActive(true);
        }
        else if (obj.tag == "ExtraPoints")
        {
	        // Получение доп баллов, воспроизведение соотв. звука и удаление самого бонуса
			eatSound.volume = PlayerPrefs.GetFloat("savevolume");
            eatSound.Play(); 
            GameObject.Find("RealScore").GetComponent<ScoreForRunning>().cur_score += 100;
            Destroy(obj.gameObject);
        }
        else if (obj.tag == "Void")
        {
	        //обнуление жизней при падении в пропасть
            health = 0;
        }
        else if (obj.tag == "Enemy")
        {
	        // Получение урона при столкновении с врагов, соответсвующая анимация и звук
            health -= 1;
			bolnoSound.volume = PlayerPrefs.GetFloat("savevolume");
			anim.SetTrigger("bolno");
			bolnoSound.Play();
			// подкидывание, если была доп. жизнь
			if (health == 1)
			{
				Jump(15);
				healthIcon.SetActive(false);
			}
        }
    }
    private void Update()
    {
	    //Бесконечный бег, проверка на то, что персонаж стоит на земле
        rb.velocity = new Vector2(1 * speed, rb.velocity.y);
        is_on_ground = Physics2D.OverlapCircle(feetPos.position, checkRadius, WhatIsGround);
        
        // прыжок и соответствующий звук и анимация
        if (Input.GetMouseButtonDown(0) && is_on_ground == true)
        {
	        Jump(10);
			barkSound.volume = PlayerPrefs.GetFloat("savevolume");
			barkSound.Play(); 
            anim.SetTrigger("take_of");
        }

        //прекращение анимации прыжка при приземлении, и ее начало при пребывании в воздухе
        if (is_on_ground == true)
        {
            anim.SetBool("is_jumping", false);
        }
        else
        {
            anim.SetBool("is_jumping", true);
        }

	    //Открытие меню смерти при получении урона без доп. жизни
	    
        if (health < 1)
        {
			deathSound.volume = PlayerPrefs.GetFloat("savevolume");
			deathSound.Play();
            Game.SetActive(false);
            UserInterfaceCanvas.SetActive(false);
            DeathCanvas.SetActive(true);
        }
	
        //Обновление счета и рекорда
		score = (int)RealScoreTable.GetComponent<ScoreForRunning>().cur_score;
		record = PlayerPrefs.GetInt("savescore");
		if (score > record)
		{
			PlayerPrefs.SetInt("savescore", score);
			PlayerPrefs.Save();
		}

		//Обуление рекорда на этапе разработки
		if (Input.GetKeyDown(KeyCode.D))
		{
			PlayerPrefs.DeleteAll();
		}
   }

    /*
    public void testDestroyer(Collider2D obj)
    {
	    if (obj.tag == "batoot")
	    {
		    Destroy(obj.gameObject);
	    }
	    else if (obj.tag == "Enemy")
	    {
		    Destroy(obj.gameObject);
	    }
    }*/
}

