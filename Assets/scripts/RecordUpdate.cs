using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordUpdate : MonoBehaviour
{
    public Text RecordNumberText;
    void Update()
    {
        //Обновление значения рекорда игрока
        RecordNumberText.text = PlayerPrefs.GetInt("savescore").ToString();
    }
}
