using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portal : MonoBehaviour
{
    // Указание на парный портал
	[SerializeField] private GameObject exit;
	private AudioSource bg_sound;
	void Start()
	{
		bg_sound = GetComponent<AudioSource>();
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		//Изменение координат игрока и воспроизведение соотв. звука при вхождении в портал
		if (col.tag == "Player"){
			col.transform.position = exit.transform.position;
			bg_sound.volume = PlayerPrefs.GetFloat("savevolume");
			bg_sound.Play();
		}
	}
}
