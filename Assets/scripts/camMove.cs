using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camMove : MonoBehaviour
{
    public GameObject player;
    public float zPos;
        //Камера перемещается вместе с игроком
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x+5, player.transform.position.y, zPos);
    }
}
