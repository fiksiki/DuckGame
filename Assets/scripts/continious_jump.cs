using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class continious_jump : MonoBehaviour
{
    // прыжки утки на экране главного меню каждые 5 секунд, более подробно в player_cntrl
    public float jumpForce;
    private float moveInput;

    private Rigidbody2D rb;

    private bool is_on_ground;
    public Transform feetPos;
    public float checkRadius = 0.5f;
    public LayerMask WhatIsGround;
    public float timer = 5;
    private Animator anim;
    
    private void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }


    private void Update()
    {
        is_on_ground = Physics2D.OverlapCircle(feetPos.position, checkRadius, WhatIsGround);

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 5;
        }
        
        if (is_on_ground == true && timer == 5)
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            anim.SetTrigger("take_of");
        }

        if (is_on_ground == true)
        {
            anim.SetBool("is_jumping", false);
        }
        else
        {
            anim.SetBool("is_jumping", true);
        }
        
        

    }


}

