using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class raccoon_script : MonoBehaviour
{
    private Rigidbody2D rb;
    private bool isFacingRight = true;
    public int speedRight = 1;
    public float timePassed = 0;
    public int speed = 5;

    public int timeTillFlip = 4;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        //Отсчет времени до разворота енота
        timePassed += Time.deltaTime;
        if (timePassed > timeTillFlip)
        {
            timePassed = 0;
            Flip();
        }
        //Постоянное движение енота
        rb.velocity = new Vector2(speedRight * speed, rb.velocity.y);
    }
    void Flip()
    {
        //Разворот енота
        isFacingRight = !isFacingRight;
        speedRight = (-1) * speedRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
