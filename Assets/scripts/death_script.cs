using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class death_script : MonoBehaviour
{
    // Соответсвующие кнопки в меню смерти
    public void Restart()
    {
        SceneManager.LoadScene("Game_scene");
    } 
    public void MenuExit()
    {
        SceneManager.LoadScene("menu");
    } 
    public void ExitPressed()
    {
        Application.Quit();
    }
}