using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music_cntrl : MonoBehaviour
{
    // Настройка громкости фоновой музыки
    private AudioSource bg_sound;


    private float musicVolume = 1f;

    void Start()
    {
        bg_sound = GetComponent<AudioSource>();
    }

    void Update()
    {
        bg_sound.volume = musicVolume;
        
    }

    public void SetVolume(float volume)
    {
        musicVolume = volume;
    }
}   
