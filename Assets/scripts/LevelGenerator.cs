using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {
    //объект, создающий участи уровня по мере прохождения его игроком
    private const float PLAYER_DISTANCE_SPAWN_LEVEL_PART = 15f;

    public GameObject Game;
    public Transform levelPart_Start;
    public List<Transform> levelPartList;
    public GameObject player;
    private Vector3 lastEndPosition;
	private List<Transform> chunks = new List<Transform>();
    int startingSpawnLevelParts = 1;

    private void Awake() {
        // добавление в массив начальной части уровня
		chunks.Add(levelPart_Start);
        lastEndPosition = levelPart_Start.Find("EndPosition").position;
        
        //добавления первого случайного шаблона на уровнь
        for (int i = 0; i < startingSpawnLevelParts; i++) {
            SpawnLevelPart();
        }
    }

    private void Update() {
        // По мере прохождения, добавляются новые участки
        if (player.transform.position.x > chunks[chunks.Count - 1 ].gameObject.transform.position.x - 15) {
            
            SpawnLevelPart();
        }
    }

    private void SpawnLevelPart() {
        // перемещение маркера, указывающего где спавнить новый участок
        Transform chosenLevelPart = levelPartList[Random.Range(0, levelPartList.Count)];
        Transform lastLevelPartTransform = SpawnLevelPart(chosenLevelPart, lastEndPosition);
        lastEndPosition = lastLevelPartTransform.Find("EndPosition").position;
    }

    private Transform SpawnLevelPart(Transform levelPart, Vector3 spawnPosition) {
        //Добавление нового участка из списка
        Transform levelPartTransform = Instantiate(levelPart, spawnPosition, Quaternion.identity, Game.transform);
		chunks.Add(levelPartTransform);
        // удаление лишних(уже пройденных) участков
		if (chunks.Count >=10){
			Destroy(chunks[0].gameObject);
			chunks.RemoveAt(0);
		}
			
        return levelPartTransform;
    }

}
