using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player_volume : MonoBehaviour
{
    public float slider_volume;

    public Slider slider;
    void Start()
    {
        // Установка пользовательской громкости с прошлого сеанса
        slider.value = PlayerPrefs.GetFloat("savevolume");
    }
    
    void Update()
    {
        //Обновление значения пользовательской громкости в процессе игры
        slider_volume = slider.value;
        
        PlayerPrefs.SetFloat("savevolume", slider_volume);
        PlayerPrefs.Save();
        
        //Удаление предустановленных значений на этапе разработки
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
