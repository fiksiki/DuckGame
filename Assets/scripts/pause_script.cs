using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

 
public class pause_script : MonoBehaviour
{
    // кнопки для меню паузы
    public void MenuExit()
    {
        SceneManager.LoadScene("menu");
    } 
    public void ExitPressed()
    {
        Application.Quit();
    }
}