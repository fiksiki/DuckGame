using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;


public class TestSuitePlay 
{
    private GameObject player;
    private GameObject gameGameObject;
        

    [SetUp]
    public void Setup()
    {
        gameGameObject = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/FullScene"));
        player = GameObject.Find("chetkaya_utka");
    }
    [TearDown]
    public void Teardown()
    {
        Object.Destroy(gameGameObject);
    }
    // Проверка что утка бежит вправо
    [UnityTest]
    public IEnumerator TestUtkaMovesRight()
    {
        float xpos = player.transform.position.x;
        yield return new WaitForSeconds(0.3f);
        Assert.Less(xpos,player.transform.position.x);
    }
    // Проверка что утка прыгает
    [UnityTest]
    public IEnumerator TestUtkaJumps()
    {
        float ypos = player.transform.position.y;
        player.GetComponent<player_cntrl>().Jump(10);
        yield return new WaitForSeconds(0.2f);
        Assert.Less(ypos,player.transform.position.y);
    }
    [UnityTest]
    public IEnumerator TestUtkaDies()
    {
        yield return new WaitForSeconds(4f);
        Assert.False(GameObject.Find("chetkaya_utka"));
    }
    
    [UnityTest]
    public IEnumerator TestScoreChanging()
    {
        int start_score = (int) GameObject.Find("RealScore").GetComponent<ScoreForRunning>().cur_score;
        yield return new WaitForSeconds(1f);
        int finish_score = (int) GameObject.Find("RealScore").GetComponent<ScoreForRunning>().cur_score;
        Assert.Less(start_score,finish_score);
    }
    
    
    [UnityTest]
    public IEnumerator TestUtkaNaPikahDies()
    {
        GameObject pikes = 
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Enemies/piki_krasivye"), new Vector3(-3, 3, 10), Quaternion.identity);

        yield return new WaitForSeconds(1);
        Object.Destroy(pikes);
        Assert.False(GameObject.Find("chetkaya_utka"));
    }
    
    [UnityTest]
    public IEnumerator TestUtkaEnemyCollisionDies()
    {
        GameObject racoon = 
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Enemies/racoon"), new Vector3(-5, 3, 10), Quaternion.identity);

        yield return new WaitForSeconds(1);
        Object.Destroy(racoon);
        Assert.False(GameObject.Find("chetkaya_utka"));
    }
    
    
    [UnityTest]
    public IEnumerator TestUtkaBatootJump()
    {
        float ypos = player.transform.position.y;
        GameObject batoot = 
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Bonus/batoot"), new Vector3(-4, 3, 10), Quaternion.identity);
        yield return new WaitForSeconds(1);
        Object.Destroy(batoot);
        Assert.Less(ypos,player.transform.position.y);
    }
    
    [UnityTest]
    public IEnumerator TestUtkaScoreBonus()
    {
        GameObject extra_points = 
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Bonus/ExtraPoints"), new Vector3(-4, 3, 10), Quaternion.identity);
        int start_score = (int) GameObject.Find("RealScore").GetComponent<ScoreForRunning>().cur_score;
        yield return new WaitForSeconds(1);
        int finish_score = (int) GameObject.Find("RealScore").GetComponent<ScoreForRunning>().cur_score;
        Assert.Less(start_score,finish_score - 100);
    }
    
    [UnityTest]
    public IEnumerator TestUtkaHealthBonus()
    {
        GameObject extra_health = 
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Bonus/ExtraLife"), new Vector3(-4, 3, 10), Quaternion.identity);
        GameObject pikes = 
            MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Enemies/piki_krasivye"), new Vector3(-3, 3, 10), Quaternion.identity);
        yield return new WaitForSeconds(1);
        Object.Destroy(pikes);
        Assert.True(GameObject.Find("chetkaya_utka"));
        
    }
}